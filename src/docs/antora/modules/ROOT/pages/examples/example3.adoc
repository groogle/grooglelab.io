= Write in a sheet and send an e-mail

[source,console]
----
mkdir test

cd test

# remember download client_secret.json

nano example3.groovy
----

.example3.groovy
[source,groovy]
----
@Grab('com.puravida-software.groogle:groogle-gmail:3.0.0')
@Grab('com.puravida-software.groogle:groogle-sheet:3.0.0')

import com.puravida.groogle.*
import com.google.api.services.gmail.GmailScopes
import com.google.api.services.sheets.v4.SheetsScopes
import groovy.io.FileType

groogle = GroogleBuilder.build {
    withOAuthCredentials {
       applicationName 'test-gmail'
       withScopes SheetsScopes.SPREADSHEETS,GmailScopes.MAIL_GOOGLE_COM
       usingCredentials "client_secret.json"
    }
    service(SheetServiceBuilder.build(), SheetService)
    service(GmailServiceBuilder.build(), GmailService)
}

gmailService = groogle.service(GmailService)
sheetService = groogle.service(SheetService)


sheetService.withSpreadSheet 'THE_SHEET_ID', { //<1>
   withSheet 'Hoja 1', {
        int count = 2   //<2>
        String email
        while( email = cell("A$count").get() ){ //<3>
            println "send email to $email"
            gmailService.sendEmail {
                to email
                from 'me'
                subject B1      //<4>
                body """Hi,
                    is ${new Date()} time
                """
            }
            count++
        }
    }
}
----
<1> Replace with sheet ID
<2> Emails start from line 2
<3> Read column A in a loop
<4> Read a cell with a fixed position (only in Groovy)